# Outlier testing used for data fit to nonlinear models

# Briefly:
# Suspicious outliers are rejected if they are not consistent with a model fit to the rest of the data.
# To test this, the suspcious point or points are excluded and the remaining data fit to the approrpiate model using nonlinear least squares (nls or nlsLM).
# We then calculate the 95% prediction interval of the model at the position of the suspcious outlier via Monte Carlo bootstrapping (predict).
# For multiple outliers, a Holm-Bonferroni correction is applied when calculating the prediciton intervals. 
# Suspicious outliers outside the prediction interval are deemed inconsistent with the rest of the data and rejected.

## To use the more stringent Bonferroni correction rather than Holm-Bonferroni, change lines 61-63 to 
### while(i <= outliers)    # loop up to the number of outliers
###  {
###  pred_BF[[i]] <- predictNLS(fit_corr, newdata=data.frame(Zn=df_test$Zn[Z_order[i]]), interval = "prediction", alpha=0.05/(outliers)) 

library(propagate)

# Function used to draw 'hockey stick plots' in figures 2c, 3S1
dG_Hockey <- function(Zn, dGapo, KZn)( 
  dGapo +  0.00198 * 277 * log( 
    (1+10^Zn/KZn) / (1 + (10^Zn)/(10^-9)) 
  ))

## Figure 2c

### WT data and suspicious outliers
Zn_2cWT <- cbind(c(-11.658, -12.509, -13.155, -13.244, -13.77, -14.092, -14.745, -15.75,-11.658, -12.509, -13.155, -13.244, -13.77, -14.092, -14.745, -15.75))
dG_2cWT <- cbind(c(7.8257,8.1520, 7.4515, 6.7305,	6.4224,	6.5878,	6.1719,	6.1197,	7.898,	7.8457,	7.3955,	6.7953,	6.4795,	6.4685, 6.3662,	5.8688))

guess <- list(dGapo = 6, KZn = 10^-14) # Estimated fit parameters for nonlinear model

sus <- c(1,9) # indices of suspicious points

outliers <- length(sus) # number of outliers to be tested


### fit WT
df <- data.frame(Zn = Zn_2cWT, dG = dG_2cWT)
fit <-  nls(dG ~ dG_Hockey(Zn, dGapo, KZn), data=df, start=guess )

#### re-fit without putative outliers
df_corr <- data.frame(Zn=df$Zn[-sus], dG = df$dG[-sus])
df_test <- data.frame(Zn=df$Zn[sus], dG = df$dG[sus])
fit_corr <-  nls(dG ~ dG_Hockey(Zn, dGapo, KZn), data=df_corr, start=guess )


### Test the outliers
pred <- predictNLS(fit_corr, newdata=df_test["Zn"], interval = "prediction")

#### Holm-Bonferroni correction for multiple outliers

  Z <- (df_test$dG - pred$summary[,7])/pred$summary[,8] # Z-scores of suspicious outliers
  Z_order <- order(abs(Z), decreasing=TRUE) # Lacking p-values, suspicious outliers are tested in order of decreasing Z-values. 
  
  pred_BF <- list()  # Instead of using p-values, we calculate corrected prediction intervals 
  
  r <- vector()  # Whether or not each outlier should be rejected, in order of Z values
  reject <- vector()  # Whether or not each outlier should be rejected, in order they were given in 'sus'
  bounds <- list() # the confidence intervals against which outliers are tested, in the order they were given in 'sus'
  
  i <- 1  
  while(!any(r==FALSE) & i <= outliers)    # loop while all points tested have been successfully rejected, up to the number of outliers
  {
    pred_BF[[i]] <- predictNLS(fit_corr, newdata=data.frame(Zn=df_test$Zn[Z_order[i]]), interval = "prediction", alpha=0.05/(outliers+1-i)) 
    bounds[[Z_order[i]]] <- c(pred_BF[[i]]$summary[11],pred_BF[[i]]$summary[12])
    r[i] <- df_test$dG[Z_order[i]] < bounds[[Z_order[i]]][1] | bounds[[Z_order[i]]][2] < df_test$dG[Z_order[i]]
    i <- i+1
  }

if(i<=outliers)  r[i:outliers] <- FALSE  # Retain all outliers with lower Z-values than the first outlier not rejected
  
reject[Z_order] <- r # Whether or not each outlier should be rejected, in the order given in 'sus'

df_final <- data.frame(Zn=df$Zn[-sus[reject]], dG = df$dG[-sus[reject]])  # Reconstruct the final data set excluding only the rejected outliers
fit_final <- nls(dG ~ dG_Hockey(Zn, dGapo, KZn), data=df_final, start=guess )  # Re-fit with the final data set


### Confirm goodness of fit improvement

#### Method 1: Standard Error of Parameters
#### We prefer this because the parameters are usually all we're after

RSE_original <- mean(
  summary(fit)$parameters[,2]/
    summary(fit)$parameters[,1])  # Average relative standard error of the parameters of the original fit

RSE_final <- mean(
  summary(fit_final)$parameters[,2]/
    summary(fit_final)$parameters[,1])  # Average relative standard error of the parameters of the fit with only inconsistent outliers rejected

if(RSE_original < RSE_final)  # If the relative standard error of the parameters increases in the new fit, do not reject any outliers
  reject <- replace(reject, values=FALSE)

#### Method 2: Mean Standard Error of Regression
#### NOT RUN 

# MSE_original <- summary(fit)$sigma/summary(fit)$df[2]  # Mean standard error of the original fit

# MSE_final <- summary(fit_final)$sigma/summary(fit_final)$df[2]  # Mean standard error of the fit with only inconsistent outliers rejected

# if(MSE_original < MSE_final)  # If the mean standard error of regression increases in the new fit, do not reject any outliers
#   reject <- replace(reject, values=FALSE)

  
### Display the results

df_new <- data.frame(Zn=seq(from=round(min(df$Zn)), to=round(max(df$Zn)), by=0.1))  #  dummy x data for curve prediction
df_new <- cbind(df_new, predict(fit_final, newdata=df_new))  # predict the full curve from the model

plot(df_new, type="l", ylab=names(df)[2])
points(df)

for(i in 1:outliers)
{
  if(reject[i]==TRUE)  # For rejected points, show error bars and cross out the point in red
    {
    arrows(x0=df$Zn[sus][i], x1=df$Zn[sus][i], 
      y0=bounds[[i]][[1]], y1=bounds[[i]][[2]], 
      code=3, angle=90, length=0.05)
    points(df$Zn[sus][i],df$dG[sus][i], pch=4, col="red")
    print(paste("Reject suspicious outlier:",df$Zn[sus][i],",",df$dG[sus][i]))
  }
  else {  # For retained points, highlight in green
    points(df$Zn[sus][i],df$dG[sus][i], pch=19, col="green")
    print(paste("Retain suspicious outlier:",df$Zn[sus][i],",",df$dG[sus][i]))
  }
}

